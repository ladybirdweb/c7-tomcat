Docker container: CentOS 7 + Java 8 + Tomcat 8

## Building the image

```sh
docker build -t ladybird/c7-tomcat .
```

## How to use
Put your WAR projects under `/opt/tomcat/webapps` directory (or any other as long as you make certain to adjust the values accordingly) and run the following command

```sh
_EXPOSED_PORT=80;
_TOMCAT_BASE_DIR=/opt/tomcat;
_TOMCAT_WEBAPPS_DIR=webapps;
_TOMCAT_LOGS_DIR=logs;

mkdir -p ${_TOMCAT_BASE_DIR}/${_TOMCAT_WEBAPPS_DIR} && \
mkdir -p ${_TOMCAT_BASE_DIR}/${_TOMCAT_LOGS_DIR}

docker run \
  -p ${_EXPOSED_PORT}:8080 \
  -v ${_TOMCAT_BASE_DIR}/${_TOMCAT_WEBAPPS_DIR}:/opt/tomcat/webapps \
  -v ${_TOMCAT_BASE_DIR}/${_TOMCAT_LOGS_DIR}:/opt/tomcat/logs \
  -it --name c7-tomcat ladybird/c7-tomcat
```

Once you run it, you can start the container with `docker start c7-tomcat` in next time and log file will be under the `/opt/tomcat/logs` directory.

Also, if you got some error, you can remove the container with `docker rm c7-tomcat`. Your current container list will be show with `docker ps -a`.

For Mac users, you must share the directory `/opt/tomcat/webapps` and `/opt/tomcat/logs` on Docker > Preferences > File Sharing.

## Versions
If you get errors building image, please check the latest version of Java and Tomcat and ajust accordingly.

|Software|Version|Note|
|:-----------|:------------|:------------|
|CentOS|7||
|Java|8u151|[Java Release Note](http://www.oracle.com/technetwork/java/javase/8all-relnotes-2226344.html#R180_151)|
|Apache Tomcat|8.5.24|[Tomcat Download Page](http://tomcat.apache.org/download-80.cgi#8.5.24)|

[Docker Official Image for Tomcat](https://github.com/docker-library/tomcat) is also available.
