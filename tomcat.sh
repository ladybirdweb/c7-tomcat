#!/bin/bash
[[ ! -f ${CATALINA_HOME}/scripts/.tomcat_admin_created ]] && ${CATALINA_HOME}/scripts/create_admin_user.sh
exec catalina.sh run
